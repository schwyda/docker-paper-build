ARG UBUNTU_VERSION=latest
FROM ubuntu:${UBUNTU_VERSION}

ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get install -y \
    texlive-full \
    make \
    librsvg2-bin \
    && rm -rf /var/lib/apt/lists/*

